require('dotenv').config();

const express = require('express');
const app = express();
app.use(express.json());

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}
app.use(enableCORS);

const io = require('./io');
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');
const transactionController = require('./controllers/TransactionController');

const port = process.env.PORT || 3000;
app.listen(port);

console.log("API escuchando en el puerto: " + port);

app.get("/apitechu/v2/users", userController.getUsersV2);
app.get("/apitechu/v2/users/:id", userController.getUserByIdV2);
app.post("/apitechu/v2/users", userController.createUserV2);

app.post("/apitechu/v2/login", authController.loginV2);
app.post("/apitechu/v2/logout",authController.logoutV2);

app.get("/apitechu/accounts/:id", accountController.accounts);
app.get("/apitechu/account/:id", accountController.account);
app.post("/apitechu/accounts/:id", accountController.createAccount);
app.get("/apitechu/currencies/:id", accountController.currencies);

app.get("/apitechu/accounts/:id/transactions", transactionController.transactions);
app.post("/apitechu/accounts/:id/transactions", transactionController.createTransaction);

// app.post("/apitechu/v1/monstruo/:p1/:p2",
//   function(req, res){
//     console.log("GET /apitechu/v1/monstruo/:p1/:p2");
//
//     console.log("Parámetros:");
//     console.log(req.params);
//
//     console.log("Query string:");
//     console.log(req.query);
//
//     console.log("Headers:");
//     console.log(req.headers);
//
//     console.log("Body:");
//     console.log(req.body);
//   }
// )
