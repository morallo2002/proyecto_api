# Dockerfile APITECHU

# Imagen raiz
FROM node

#Carpeta trabajo
WORKDIR /apitechu

# Añado archivos de mi aplicación a imagen
ADD . /apitechu

# Instalo los paquetes necesarios. RUN se ejecuta en el momento que se está creando la imagen
RUN npm install

#Abrir puerto de la API
EXPOSE 3000

# Comando de inicialización. CMD se ejecuta cuando se ha construído la imagen
CMD ["npm", "start"]
