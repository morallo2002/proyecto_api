const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8mao/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV2(req, res) {
 console.log("POST /apitechu/v2/login");
 var query = 'q={"email": "' + req.body.email + '"}';

 httpClient = requestJson.createClient(mlabBaseURL);

 httpClient.get("user?" + query + "&" + mlabAPIKey,
   function(err, resMLab, body) {
     if (body.length <= 0) {
       var response = {
         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
       }
       res.status(401); //Unathorized
       res.send(response);
     } else {
       var isPasswordcorrect = crypt.checkpassword(req.body.password, body[0].pwd);

       if (!isPasswordcorrect) {
         var response = {
           "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
         }
         res.status(401); //Unathorized
         res.send(response);
       } else {
         query = 'q={_id:{$oid:"'+body[0]._id.$oid+'"}}';

         var putBody = '{"$set":{"logged":true}}';
         httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             var response = {
               "msg" : "Usuario logado con éxito",
               "idUsuario" : body[0]._id.$oid
             }
             res.send(response);
           }
         )
       }
     }
   }
 );

}


function logoutV2(req, res) {
 console.log("POST /apitechu/v2/logout/:id");
 var query = 'q={_id:{$oid:"'+req.body.id+'"}}';

 httpClient = requestJson.createClient(mlabBaseURL);
 httpClient.get("user?" + query + "&" + mlabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       var putBody = '{"$unset":{"logged":""}}'

       httpClient.put("user?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0]._id.$oid
           }
           res.send(response);
         }
       )
     }
   }
 );
}

module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
