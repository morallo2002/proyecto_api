const requestJson = require('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8mao/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function transactions (req, res) {
  console.log("GET /apitechu/accounts/{accountid}/transactions");

  var id = req.params.id;
  var query = 'q={"idAccount": "'+id+'"}&sort={"date":-1}';
  var httpClient = requestJson.createClient(mlabBaseURL);

  httpClient.get("transaction?" + query +"&" + mlabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {
          "msg" : "Error obteniendo transacciones"
        };
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {
            "msg" : "transacciones no encontradas"
          };
          res.status(404);
        }
      }

      res.send(response);
    }
  )
}

function createTransaction (req, res){
  console.log("POST /apitechu/accounts/{accountid}/transactions");

  var httpClient = requestJson.createClient(mlabBaseURL);

  var newTransaction = {
    "date" : formattedDate(new Date()),
    "amount" : req.body.amount,
    "description" : req.body.description,
    "idAccount" : req.params.id
  };

  httpClient.post("transaction?" + mlabAPIKey, newTransaction,
    function(err, resMLab, body) {

      var query = 'q={_id:{$oid:"'+req.params.id+'"}}';
      var httpClient2 = requestJson.createClient(mlabBaseURL);
      httpClient2.get("account?" + query +"&" + mlabAPIKey,
        function(err2, resMLab2, body2) {
          var auxAccount = body2[0];
          auxAccount.balance += req.body.amount;

          var httpClient3 = requestJson.createClient(mlabBaseURL);
          httpClient3.put("account?" + query +"&" + mlabAPIKey, auxAccount,
            function(err3, resMLab3, body3) {
              // console.log("Saldo cuenta actualizado");
              var response = {
                "msg" : "Transacción creada con éxito",
                "userId" : auxAccount.userId
              };
              res.status(201);
              res.send(response);
            }
          )
        }
      )

    }
  )
}

function formattedDate(d) {
  let month = String(d.getMonth() + 1);
  let day = String(d.getDate());
  const year = String(d.getFullYear());

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return `${day}/${month}/${year}`;
}


module.exports.transactions = transactions;
module.exports.createTransaction = createTransaction;
