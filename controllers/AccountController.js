const requestJson = require('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8mao/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

const forexURL = "https://forex.1forge.com/1.0.3/";
const forexAPIKey = process.env.FOREX_API_KEY;


/**
Recupera los datos de una cuenta
*/
function account (req, res) {
  console.log("GET /apitechu/account/:id");

  var id = req.params.id;
  var query = 'q={_id:{$oid:"'+id+'"}}';

  var httpClient = requestJson.createClient(mlabBaseURL);

  httpClient.get("account?" + query +"&" + mlabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {
          "msg" : "Error obteniendo cuenta"
        };
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
        } else {
          var response = {
            "msg" : "Cuenta no encontrada"
          };
          res.status(404);
        }
      }

      res.send(response);
    }
  )
}

/**
Recupera las cuentas de un usuario
*/
function accounts (req, res) {
  console.log("GET /apitechu/accounts/:id");

  var id = req.params.id;
  var query = 'q={"userId": "'+id+'"}';

  var httpClient = requestJson.createClient(mlabBaseURL);
  // console.log("query: " + query);

  httpClient.get("account?" + query +"&" + mlabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {
          "msg" : "Error obteniendo cuentas"
        };
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {
            "msg" : "cuentas no encontradas"
          };
          res.status(404);
        }
      }

      res.send(response);
    }
  )
}

function createAccount (req, res){
  console.log("POST /apitechu/accounts/id ");

  var httpClient = requestJson.createClient(mlabBaseURL);

  var newAccount = {
    "IBAN" : "ES990999" + "1111" + "0020" + getRandomInt(1, 99999999),
    "alias" : req.body.alias,
    "divisa" : req.body.divisa,
    "balance" : 0,
    "userId" : req.params.id
  };

  httpClient.post("account?" + mlabAPIKey, newAccount,
    function(err, resMLab, body) {
      res.status(201);

      var response = {
        "msg" : "Cuenta creada correctamente",
        "userId" : req.params.id
      }
      res.send(response);
    }
  )
}

function currencies (req, res) {
  // console.log("GET https://forex.1forge.com/1.0.3/quotes?pairs=EURUSD,EURGBP,EURJPY,EURCHF&api_key=xxxxxx");

  const ForexDataClient = require("forex-quotes");
  let client = new ForexDataClient(forexAPIKey);

  var id = req.params.id;

  client.getQuotes([id]).then(response => {
    // console.log(response);
    res.send(response);
  });
}

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

module.exports.accounts = accounts;
module.exports.account = account;
module.exports.createAccount = createAccount;
module.exports.currencies = currencies;
