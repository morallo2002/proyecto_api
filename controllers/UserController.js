const io = require('../io');
const crypt = require('../crypt');

const requestJson = require('request-json');
const mlabBaseURL = "https://api.mlab.com/api/1/databases/apitechu8mao/collections/";
const mlabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getUsersV2 (req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(mlabBaseURL);

  httpClient.get("user?" + mlabAPIKey,
    function(err, resMLab, body) {
      var response = !err ? body : {"msg" : "Error obteniendo usuarios"}

      res.send(response);
    }
  )
}

function getUserByIdV2 (req, res) {
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={_id:{$oid:"'+id+'"}}';

  var httpClient = requestJson.createClient(mlabBaseURL);

  httpClient.get("user?" + query +"&" + mlabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        };
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          };
          res.status(404);
        }
      }

      res.send(response);
    }
  )
}


function createUserV2 (req, res){
  console.log("POST /apitechu/v2/users ");

  // =============================================
  // Validamos que el email no está ya registrado
  var query = 'q={"email": "' + req.body.email + '"}';

  var httpClient = requestJson.createClient(mlabBaseURL);

  httpClient.get("user?" + query +"&" + mlabAPIKey,
    function(err, resMLab, body) {
      if (body.length > 0) {
        // console.log("email existente!!");

        res.status(200);
        res.send({"msg" : "Email registrado para otro usuario"});
      } else {
        var newUser = {
          "email" : req.body.email,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "pwd" : crypt.hash(req.body.pwd),
          "address" : req.body.address,
          "city" : req.body.city,
          "state" : req.body.state,
          "zip" : req.body.zip,
          "phone" : req.body.phone,
          "movilePhone" : req.body.movilePhone,
          "logged" : false
        };

        httpClient.post("user?" + mlabAPIKey, newUser,
          function(err, resMLab, body) {
            res.status(201);

            res.send({"msg" : "Usuario registrado correctamente"});
          }
        )
      }
    }
  )
}

/*
function deleteUserV1(req, res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("La id enviada es: " + req.params.id);

  var users = require("../usuarios.json");
  //users.splice(req.params.id - 1, 1);
  var found = false;

  // ======================================================================
  // Búsqueda por método FOR
  var auxUser;

  for (var i = 0; ((i < users.length) && (!found)); i++) {
    auxUser = users[i];

    if (auxUser.id == req.params.id) {
      found = true;
      users.splice(i, 1);
    }
  }

  if (found) {
    io.writeUserDataToFile(users);
  }

  var msg = found ? "Usuario borrado" : "Usuario no encontrado";
  console.log(msg);
  res.send({"msg" : msg});
}
*/


module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV2 = createUserV2;
// module.exports.deleteUserV1 = deleteUserV1;
