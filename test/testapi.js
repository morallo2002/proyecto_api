const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
// Con esta línea lanza la API para que funcione el test.
// Así evitamos tener que levantarlo en otra consola
var server = require('../server');

describe('First test',
  function() {
      it('Test that Google works', function(done) {
        chai.request('http://www.google.com')
          .get('/')
          .end(
            function(err, res) {
              console.log("Request finished");
              console.log(err);
              //console.log(res);

              res.should.have.status(200);
              done();
            }
          )
        }
      )
  }
)

describe('Test de API de Usuarios',
  function() {
      it('Prueba que la API de usuarios responde', function(done) {
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/hello')
          .end(
            function(err, res) {
              console.log("Request finished");
              res.should.have.status(200);
              res.body.msg.should.be.eql("Hola");
              done();
            }
          )
        }
      ),
      it('Prueba que la API de usuarios devuelve una lista de usuarios correcta', function(done) {
        // Vamos a comprobar la integrida de los item de user que devuelve la lista de usuarios.
        // Asumimos que la validación consiste en comprobar email y pwd
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/users')
          .end(
            function(err, res) {
              console.log("Request finished");
              res.should.have.status(200);

              for (user of res.body.users) {
                user.should.have.property("email");
                user.should.have.property("pwd");
              }

              done();
            }
          )
        }
      )
  }
)
